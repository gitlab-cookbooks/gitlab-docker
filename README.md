gitlab-docker Cookbook
======================
This cookbook will install and configure docker containers

Attributes
----------
#### gitlab-docker::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['gitlab-docker']</tt></td>
    <td>Hash</td>
    <td>See example below</td>
    <td><tt>{}</tt></td>
  </tr>
</table>

Usage
-----
#### gitlab-docker::default
Include `gitlab-docker` in your node's `role`:

```json
{
  "name": "my_docker_role",
  "default_attributes": {
    "gitlab-docker": {
      "chef_vault": "my_docker_role",
      "minio/minio": {
        "virtualhost": "example.com",
        "port": "443",
        "docker_port": "9000",
        "ssl_certificate": "use vault!",
        "ssl_key": "use vault!"
      }
    }
  },
  "run_list": [
    "recipe[gitlab-docker]"
  ]
}
```

Contributing
------------
1. Fork the repository on GitLab
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Merge Request using GitLab

License and Authors
-------------------
Authors: Jeroen Nijhof <jeroen@gitlab.com>
