name             'gitlab-docker'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures docker containers'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.6'

depends 'gitlab-vault'
depends 'chef_nginx'
depends 'docker', '~> 2.0'
